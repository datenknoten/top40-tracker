const winston = require('winston');
const moment = require('moment');
const util = require('util');
const chalk = require('chalk');

let now = moment();

function formatter(options : any) {
    let meta;
    let _now = moment()
        .diff(now);
    let message = undefined !== options.message ? options.message : '';
    let loglevel = chalk.reset;

    if (options.level === 'debug') {
        loglevel = chalk.cyan;
    } else if (options.level === 'info') {
        loglevel = chalk.blue;
    } else if (options.level === 'warn') {
        loglevel = chalk.yellow;
    } else if (options.level === 'error') {
        loglevel = chalk.red;
    }

    if (options.meta && Object.keys(options.meta)
        .length) {
        meta = "\n" + util.inspect(options.meta, {
            colors: true,
            depth: 5,
            maxArrayLength: 200,
        });
    } else {
        meta = "";
    }

    return loglevel(`☼ ${_now} ${options.level.toUpperCase()} ${message} ${meta}`);
}

module.exports = {
    development: {
        level: 'debug',
        transports: [
            new (winston.transports.Console)({
                formatter,
            }),
        ],
        logRequests: false,
        logBody: true,
    },
    test: {
        level: 'debug',
        transports: [
            new (winston.transports.Console)({
                formatter,
            }),
        ],
        logRequests: false,
        logBody: false,
    },
    default: {
        level: 'info',
        transports: [
            new (require('winston-daily-rotate-file'))({
                timestamp: true,
                json: true,
                filename: process.cwd() + '/static/backend.log',
            }),
        ],
        logRequests: true,
        logBody: false,
    },
};
