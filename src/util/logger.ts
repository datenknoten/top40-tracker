const loggerConfig = require('../../config/logger');

import * as winston from 'winston';

export type logger = winston.LoggerInstance;

function getLoggerConfig() {
  const env = process.env.NODE_ENV;

  let config = loggerConfig[env];

  if (!config) {
    config = loggerConfig.default;
  }

  return config;
}

export function getLogger() : logger {
  let config = getLoggerConfig();
  
  let logger = new winston.Logger({
    level: config.level,
    transports: config.transports,
  });

  return logger;
}


