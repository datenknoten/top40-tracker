import {
    Config
} from '../../config/app';

import * as superagent from "superagent";

export abstract class Base {
    public Id? : number;
    public createdAt: Date;
    public lastModifiedAt: Date;

    public abstract getModelName() : string;
    public abstract toObject(): Object;

    public async Save() : Promise<void> {
        const url = `${Config.BaseUrl}/models/Top40TrackerMain/${this.getModelName()}`;

        let res = await superagent
            .agent()
            .post(url)
            .auth(Config.User, Config.Password)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .set('X-apiomat-system', Config.System)
            .set('X-apiomat-apikey', Config.APIKey)
            .send(this.toObject());   

        res = await superagent
            .agent()
            .get(res.get('Location'))
            .auth(Config.User, Config.Password)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .set('X-apiomat-system', Config.System)
            .set('X-apiomat-apikey', Config.APIKey);

        const body = res.body;

        this.Id = body.Id;
        this.createdAt = new Date(body.createdAt);
        this.lastModifiedAt = new Date(body.lastModifiedAt);
    }
}