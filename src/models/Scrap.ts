import {
    Base
} from '.';

export class Scrap extends Base {
    public body : string;

    public getModelName() : string {
        return 'Scrap';
    }

    public toObject() : Object {
        return {
            '@type': 'Top40TrackerMain$Scrap',
            body: this.body,
        };
    }
}