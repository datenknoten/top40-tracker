import {
    Base,
    Scrap
} from '.';

import {
    Config
} from '../../config/app';

import * as superagent from "superagent";

import * as moment from 'moment';

export class PlayedSong extends Base {
    public artist : string;
    public song : string;
    public playedAt: moment.Moment;
    public Scrap?: Scrap;

    public getModelName() : string {
        return 'PlayedSong';
    }

    public toObject(): Object {
        if (!this.Scrap) {
            throw new Error('No scrap for this song');
        }

        return {
            '@type': 'Top40TrackerMain$PlayedSong',
            artist: this.artist,
            song: this.song,
            playedAt: this.playedAt.toISOString(),
            scrap: this.Scrap.Id,
        }
    }

    public async Exists(): Promise<boolean> {
        const url = `${Config.BaseUrl}/models/Top40TrackerMain/${this.getModelName()}`;

        const res = await superagent
            .agent()
            .get(url)
            .query({
                q: `playedAt == date(${this.playedAt.valueOf()})`
            })
            .auth(Config.User, Config.Password)
            .set('Accept', 'application/json')
            .set('X-apiomat-system', Config.System)
            .set('X-apiomat-apikey', Config.APIKey);

        const body = res.body;

        return body.length === 1;

    }

}