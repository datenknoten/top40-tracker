import * as superagent from "superagent";
import * as cheerio from "cheerio";
import * as moment from 'moment';

import {
    AllHtmlEntities
} from 'html-entities';

import {
    getLogger
} from "./util/logger";

import {
    Scrap,
    PlayedSong,
} from './models';

const logger = getLogger();

type Moment = moment.Moment;



function parseTime(node : Cheerio) : Moment {
    const timestring = node
        .find('p')
        .html();

    const timeResults = timestring.match(/(\d\d\.\d\d) um (\d\d:\d\d)/);

    if (!timeResults) {
        throw new Error('Failed to parse timeresults');
    }

    const date = timeResults[1];
    const time = timeResults[2];
    const fullDate = moment(`${date} ${time}`, 'DD.MM HH:mm');

    return fullDate;
}

function parseArtist(node: Cheerio): PlayedSong {
    const retval = new PlayedSong();

    const songString = node
        .find('h3')
        .html();

    let work = songString.split('<br>');

    const entities = new AllHtmlEntities();

    retval.song = entities.decode(work[1].toLowerCase());
    retval.artist = entities.decode(node.find('h3 span').html().toLowerCase());

    return retval;
}

(async() => {
    try {
        let counter = {
            new: 0,
            general: 0
        };
        const res = await superagent
            .agent()
            .get('http://www.radiotop40.de/www/musik/playlist/');
        const scrap = new Scrap();
        scrap.body = res.text;

        await scrap.Save();

        const queryInterface = cheerio.load(scrap.body);

        let node = queryInterface('div.albumListPlaylist li');

        do {
            if (node.html() === null) {
                break;
            }
            const time = parseTime(node);
            const song = parseArtist(node);
            song.playedAt = time;
            song.Scrap = scrap;

            const exists = await song.Exists();

            if (!exists) {
                await song.Save();
                counter.new += 1;
            }
            counter.general += 1;            
        } while(node = node.next())
        

        logger.info(`Parsed ${counter.general} songs. From these there were ${counter.new} „new“ songs.`);

    } catch (err) {
        logger.error(`[top40tracker] errored: ${err.stack}`);

        if (err.response) {
            console.dir(err.response.text);
        }


        process.exit(1);
    }
})();
